const logInfo = require('debug')('dashboard-api:server:models:mongo:index:log-info')
const logError = require('debug')('dashboard-api:server:models:mongo:index:log-error')

const mongoose = require('mongoose')
mongoose.Promise = require('bluebird')
// mongoose.connect('mongodb://localhost/myappdatabase')

module.exports = Mongo

// readyState:
// 0 = disconnected
// 1 = connected
// 2 = connecting
// 3 = disconnecting
function Mongo() {
    if (mongoose.connection.readyState === 0) {
        connect()
    }
    return mongoose
}

function connect() {
    try {
        logInfo(Mongo.name, 'connecting to mongodb ...')
        const connection = mongoose.connect("mongodb://localhost/test")
        return connection
    } catch (error) {
        logError(Mongo.name, error)
        throw error
    }
}