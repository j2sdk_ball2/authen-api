const logInfo = require('debug')('authen-api:server:startup:log-info')
const logError = require('debug')('authen-api:server:startup:log-error')

const koa = require('koa')
const app = new koa()
const cors = require('koa-cors')
const koaRouter = require('koa-router')()
const bodyParser = require('koa-bodyparser')
const validator = require('koa-validate')

const env = require('./config/env')

module.exports = startup

function startup(models) {
  logInfo(startup.name)

  app.use(cors({
    origin: "http://localhost:8080",
    methods: 'GET,HEAD,PUT,POST,DELETE,PATCH'
  }))
  validator(app)
  app.use(bodyParser())

  require('./routers/router')(koaRouter)

  app.use(koaRouter.routes())
  app.use(koaRouter.allowedMethods())
  app.on('error', async (error) => {
    logError(startup.name, error)
  })

  const port = env.port
  const listener = app.listen(port)
  console.log(`*** authen-api is running on port ${port} ***`)

  return {
    app,
    listener
  }
}