const User = require('../models/mongo/user')()
const statusCodeMap = {
    11000: 403
}

module.exports = async (ctx, next) => {

    const body = ctx.request.body
    const email = body.email
    const password = body.password

    const user = await User.find({
        email,
        password
    }).exec()

    ctx.status = (user.length > 0) ? 200 : 403
    ctx.body = {
        user
    }
    return next()
}