module.exports = async (ctx, next) => {
    ctx.checkBody('email')
        .notEmpty('empty')
        .isEmail("invalid format")

    ctx.checkBody('password')
        .notEmpty('empty')
        .len(8, 50, {
            min: 8,
            max: 50
        })

    if (ctx.errors) {
        ctx.status = 400
        return ctx.body = ctx.errors
    }

    return next()
}