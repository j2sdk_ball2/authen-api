function* generator() {
    yield 'f'
    yield 'o'
    yield 'o'
}

const g = generator()
// console.log([...g])

function* generator1() {
    yield 'p'
    console.log('o')
    yield 'n'
    console.log('y')
    yield 'f'
    console.log('o')
    yield 'o'
    console.log('!')
}
var foo = generator1()
// for (let pony of foo) {
//   console.log(pony)
// }
// console.log([...foo])

function* generator2() {
    yield 'p'
    console.log('o')
    yield 'n'
    console.log('y')
    yield 'f'
    console.log('o')
    yield 'o'
    console.log('!')
}
const g2 = generator2()
// while (true) {
//   let item = g2.next()
//   if (item.done) {
//     break
//   }
//   console.log(item.value)
// }
// let item = g2.next()
// console.log(item.value)

let item = g2.next()
console.log(item)

item = g2.next()
console.log(item)

item = g2.next()
console.log(item)

item = g2.next()
console.log(item)

// item = g2.next()
// console.log(item)

// item = g2.next()
// console.log(item)