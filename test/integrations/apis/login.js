const request = require('supertest')
const { expect } = require('chai')

let server

describe('integration test', () => {
    describe('#login', () => {

        const url = '/api/v1/login'

        before(async () => {
            server = await require('../startup')()
        })

        it('Should return 400 (Bad Request) with empty email', async () => {

            const response = await request(server.instance)
                .post(url)
                .send({
                    email: '',
                    password: ''
                })
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .expect(400)
                .expect(response => {
                    const body = JSON.parse(response.text)
                    const item = body[0]

                    console.log(`item: ${JSON.stringify(item)}`)
                    expect(item).eql({ email: 'empty' })
                })
        })

        it('Should return 400 (Bad Request) with invalid email format', async () => {

            const response = await request(server.instance)
                .post(url)
                .send({
                    email: 'x@y.c',
                    password: ''
                })
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .expect(400)
                .expect(response => {
                    const body = JSON.parse(response.text)
                    const item = body[0]

                    console.log(`item: ${JSON.stringify(item)}`)
                    expect(item).eql({ email: 'invalid format' })
                })
        })

        it('Should return 400 (Bad Request) with empty password', async () => {

            const response = await request(server.instance)
                .post(url)
                .send({
                    email: 'x@y.co',
                    password: ''
                })
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .expect(400)
                .expect(response => {
                    const body = JSON.parse(response.text)
                    const item = body[0]

                    console.log(`item: ${JSON.stringify(item)}`)
                    expect(item).eql({ password: 'empty' })
                })
        })

        it('Should return 400 (Bad Request) with password length 7', async () => {

            const response = await request(server.instance)
                .post(url)
                .send({
                    email: 'x@y.co',
                    password: '1234567'
                })
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .expect(400)
                .expect(response => {
                    const body = JSON.parse(response.text)
                    const item = body[0]

                    console.log(`item: ${JSON.stringify(item)}`)
                    expect(item).eql({
                        password: {
                            min: 8,
                            max: 50
                        }
                    })
                })
        })

        it('Should return 200 (OK) with correct email and password', async () => {

            const response = await request(server.instance)
                .post(url)
                .send({
                    email: 'scisee01@gmail.com',
                    password: '691a87d9454b81e98e6055b6fcf915c0'
                })
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .expect(200)
        })
    });
})