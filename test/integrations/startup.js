require('dotenv').config({
    path: '.localenv_test',
    silent: true
})

async function startup() {
    if (!startup.app || !startup.pg) {
        console.log('starting server ...')

        try {
            const instance = await require('../../src/startup')()
            console.log('starting server success')
            startup.instance = instance.listener
            return startup
        } catch (error) {
            console.log('starting server failed')
            console.log(`error: ${JSON.stringify(error)}`)
            return null
        }
    }
}

module.exports = startup